




export interface GameContext {
  timestamp: number
  gameObjects: GameObjectBase[],
}

export interface GameObjectBase {
  id: number
  type: string
  x: number
  y: number
  createAt: number
  isRemove: boolean
}

export interface GameEvent {
  timestamp: number
  forId: number
  type: string
  payload: any
}


export class GameContextMutation {

  static onInit(context: GameContext): GameContext | void {

  }

  static onInterval(context: GameContext, delta: number): GameContext {
    return context
  }

  static onClean(context: GameContext): void {

  }

  static onEvent(context: GameContext, event: GameEvent): GameContext {
    return context
  }

  static onEvents(context: GameContext, events: GameEvent[]): GameContext {
    return context
  }
}

export class GameObjectMutation {

  static onCreate(ob: GameContext, context: GameContext): GameContext | void {

  }

  static onInterval(ob: GameContext, context: GameContext): GameContext | void {
    return ob
  }

  static onDestroy(ob: GameContext, context: GameContext): void {

  }
}


export class GameController {

  readonly delta = 1000
  readonly maxHistory = 20

  histories: GameContext[] = []

  eventQueue: GameEvent[] = []

  timestamp: number = 0;
  currentTimestamp: number = 0

  onTicker(timestamp: number) {
    const currentContext = this.histories[this.histories.length - 1]
    const newGameContext = GameContextMutation.onInterval({
      gameObjects: currentContext.gameObjects,
      timestamp
    }, this.delta);

    this.histories.push(newGameContext)
  }

  onNewEvent(e: GameEvent) {
    this.eventQueue.push(e)
  }

  onLoop(timestamp: number) {
    this.timestamp = timestamp

    const oldestTime = timestamp - this.maxHistory * this.delta

    this.eventQueue = this.eventQueue
      .filter(e => e.timestamp >= oldestTime)
      .sort((e, f) => e.timestamp - f.timestamp)

    const newestTime = this.currentTimestamp

    this.histories = this.histories
      .filter(e => e.timestamp >= oldestTime && e.timestamp <= newestTime)

    for (let event of this.eventQueue.filter(e => e.timestamp >= this.currentTimestamp)) {

    }


    console.log("\n".repeat(80));
    console.log(this.histories)
  }


  start() {
    var timestamp = 0;

    this.histories.push({
      gameObjects: [],
      timestamp,
    })

    setInterval(() => {
      timestamp += this.delta;
      this.onLoop(timestamp);
    }, this.delta)
  }
}
//nodemon  --exec ts-node -r tsconfig-paths/register test.ts
new GameController().start()