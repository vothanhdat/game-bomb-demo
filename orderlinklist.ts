

export class LinkListNode<T extends { timestamp: number }> {


  constructor(
    public data: T,
    public prev: LinkListNode<T> | null,
    public next: LinkListNode<T> | null,
  ) {

  }

  getOldestUtil(timestamp: number): LinkListNode<T> {
    var current: LinkListNode<T> = this;

    if (timestamp < current.data.timestamp)
      while (current.prev && current.prev.data.timestamp >= timestamp)
        current = current.prev
    else {
      while (current.next && current.next.data.timestamp < timestamp)
        current = current.next
      current = current.next || current
    }

    return current
  }

  getNewestUtil(timestamp: number): LinkListNode<T> {
    var current: LinkListNode<T> = this;

    if (timestamp > current.data.timestamp)
      while (current.next && current.next.data.timestamp <= timestamp)
        current = current.next
    else {
      while (current.prev && current.prev.data.timestamp > timestamp)
        current = current.prev
      current = current.prev || current
    }

    return current
  }

  insert(ob: T) {
    var current: LinkListNode<T> = this;
    if (ob.timestamp >= current.data.timestamp) {
      const nodeToAppend = this.getNewestUtil(ob.timestamp)
      const currentNext = nodeToAppend.next
      const newNode = new LinkListNode(ob, nodeToAppend, currentNext)
      nodeToAppend.next = newNode
      if (currentNext)
        currentNext.prev = newNode
    } else if (ob.timestamp < current.data.timestamp) {
      const nodeToInsert = this.getOldestUtil(ob.timestamp)
      const currentPrev = nodeToInsert.prev
      const newNode = new LinkListNode(ob, currentPrev, nodeToInsert)
      nodeToInsert.prev = newNode
      if (currentPrev)
        currentPrev.next = newNode
    }
  }

  toArray() {
    var current: LinkListNode<T> | null = this

    while (current && current.prev)
      current = current.prev

    var ar = []
    do {
      current && ar.push(current.data)
      current = current && current.next
    } while (current)
    return ar
  }
}

// var testlinklist = new LinkListNode<{ timestamp: number }>(
//   { timestamp: 0 },
//   null,
//   null
// )

// console.time("insert")

// for (var i = 0; i < 10000; i++)
//   testlinklist.insert({ timestamp: Math.random() * 10000 })

// console.timeEnd("insert")

// console.time("t")
// for (var i = 0; i < 100000; i++) {
//   testlinklist.getNewestUtil(Math.random() * 10000).getNewestUtil(Math.random() * 10000)
// }
// console.timeEnd("t")