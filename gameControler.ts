




export interface GameContext {
  timestamp: number
  gameObjects: GameObjectBase[],
}

export interface GameObjectBase {
  id: number
  type: string
  x: number
  y: number
  createTime: number
  isRemove: boolean
}

export interface GameEvent {
  forId: number
  type: string
  payload: any
}


export class GameContextMutation<T extends GameContext> {

  onCreate(ob: T, context: GameContext): T | void {

  }

  onInterval(ob: T, context: GameContext): T | void {
    return ob
  }

  onDestroy(ob: T, context: GameContext): void {

  }
}



export class GameObjectMutation<T extends GameObjectBase> {

  onCreate(ob: T, context: GameContext): T | void {

  }

  onInterval(ob: T, context: GameContext): T | void {
    return ob
  }

  onDestroy(ob: T, context: GameContext): void {

  }
}

