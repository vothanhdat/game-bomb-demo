///<reference path="./global.d.ts" />
import {Screen as GameScreen} from './GameScreen'
import * as GameObject from './GameObject'

var screen = new GameScreen()
var canvas = document.createElement('canvas')
var context = canvas.getContext('2d')

canvas.width = GameObject.GameObjectBase.basePixelSize * 10
canvas.height = GameObject.GameObjectBase.basePixelSize * 10

console.log(GameObject)

function onRender(){
    context.clearRect(0,0,canvas.width,canvas.height)
    screen.render(context);
    screen.update(16);
    requestAnimationFrame(onRender);
}

requestAnimationFrame(onRender);


document.body.appendChild(canvas)








