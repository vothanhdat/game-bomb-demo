
export interface Context {
    timestamp: number
    gameObjects: GameObjectBase[],
}


export interface GameEvent {
    forId: number
    type: string
    payload: any
}


export interface GameObjectBase {
    id: number
    type: string
    x: number
    y: number
    createTime: number
    isRemove: boolean
}

export class GameObjectMutation<T extends GameObjectBase> {

    onCreate(ob: T, context: any): T {
        return ob
    }

    onInterval(ob: T, context: any): T {
        return ob
    }

    onDestroy(ob: T, context: any): void {

    }
}

