import {StaticOb as base}  from './base'
import {loadImage} from '../../utilities'

class Stone extends base {
    static readonly texture = loadImage('assets/stone.png')
    constructor(p : Point){
        super(p)
        this.texture = Stone.texture
    }
}

export default Stone