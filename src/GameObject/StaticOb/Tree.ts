import {StaticOb as base}  from './base'
import {loadImage} from '../../utilities'

class Tree extends base {
    static texture = loadImage('assets/tree.png');
    constructor(p : Point){
        super(p)
        this.texture = Tree.texture
    }
}

export default Tree