
import GameScreen from '../GameScreen/Screen'

let id = 1000;


export class GameObjectBase {
    static basePixelSize = 60

    x: number

    y: number

    type: string

    lifetime: number

    isRemove: boolean
    texture: HTMLImageElement
    context: GameScreen
    constructor(p: Point) {
        this.type = this.constructor.name
        this.x = p.x
        this.y = p.y
        this.lifetime = 0
    }
    render(context: CanvasRenderingContext2D): void {
        const basePixelSize = GameObjectBase.basePixelSize
        context.drawImage(this.texture, this.x * basePixelSize, this.y * basePixelSize, basePixelSize, basePixelSize)
    }
    update(time: number): void {
        this.lifetime += time
    }
    setGameContext(context : GameScreen){
        this.context = context
    }
    delete(){
        this.isRemove = true
    }
}
