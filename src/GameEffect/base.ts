import GameContext from '../GameScreen/Screen'

class BaseEffect {
    static basePixelSize = 60
    x: number
    y: number
    lifetime: number
    isRemove: boolean
    texture: HTMLImageElement
    context: GameContext
    constructor(p: Point) {
        this.x = p.x
        this.y = p.y
        this.lifetime = 0
    }
    render(context: CanvasRenderingContext2D): void {

    }
    update(time: number): void {
        this.lifetime += time
    }
    setGameContext(context : GameContext){
        this.context = context
    } 
}

export default BaseEffect