export function loadImage(src : string) : HTMLImageElement {
    let img = new Image
    img.src = src
    return img
}


export function fetchText(src : string) : Promise<string> {
    return fetch(src)
        .then(response => response.text() )
}


export function addInRange(start : number,end : number,value : number) : number {
    if(start < end){
        return Math.min(Math.max(start,start + Math.abs(value)),end)
    }else{
        return Math.min(Math.max(end,start - Math.abs(value)),start)
    }
}

export function numIsEqual(num1 : number, num2 : number,approximation : number) : boolean {
    return Math.abs(num1 - num2) < approximation
}


declare global {
  interface Array<T> {
    remove(elem: T): void;
  }
}


Array.prototype.remove = function(item : any){
    var itemIndex = this.findIndex((e : any) => e == item)
    if(itemIndex > -1 && itemIndex < this.length - 1){
        var item = this.pop()
        this[itemIndex] = item
    }else if (itemIndex == this.length - 1){
        this.pop()
    }
}